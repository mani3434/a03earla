QUnit.test('Testing Multiplication function with four sets of inputs', function (assert) {
    assert.throws(function () { getBmi(); }, new Error("only numbers are allowed"), 'Passing in array correctly raises an Error');
    assert.strictEqual(getBmi(0,0), 32, 'All are null values');
    assert.strictEqual(getBmi(150,70), 8, 'All positive numbers');
    assert.strictEqual(getBmi(-150,70), -18, 'Negative and positive numbers');
    assert.strictEqual(getBmi(-150,-70), 32, 'All are negative numbers');
    
});
