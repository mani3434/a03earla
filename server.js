var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");
var ejs  = require('ejs');
var app = express();  // make express app
var server = require('http').Server(app); // pass in our http app server to get a Socket.io server
//1var path = require('path');
 app.get('/', function(req, res){
 app.use(express.static(path.join(__dirname)));
 res.sendFile(path.join(__dirname, '../A03Earla/assets', 'Earla.html'));
 // app.use(express.static(__dirname + '/assets/'));
});

 //adding css
app.use(express.static(__dirname+'/assets/'));

 // set up the view engine
 //app.use(express.static(__dirname + '/assets/'));
app.set("views", path.resolve(__dirname, "views")); // path to assets
app.set("view engine", "ejs"); // specify our view engine

 


// manage our entries
var entries = [];
app.locals.entries = entries;

// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));


// on a GET request to default page, do this.... 

 


// http GET (default and /new-entry)
app.get("/", function (request, response) {
  response.render("index");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});
app.get("/index", function (request, response) {
response.render("index");
});


// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/index");
});

// 404
app.use(function (request, response) {
  response.status(404).render("404");
});
 
// Listen for an app request on port 8081
server.listen(8081, function(){
  console.log('listening on http://127.0.0.1:8081/');
});