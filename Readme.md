# A03Earla
Here I created a website to calculate Body Mass Indix.
For that I took weight and height as inputs.
#Abstract
Calculate Body Mass Index(BMI) to know wether a person is healthy or not by taking inputs of his/her Weight and Body.
Formula for calculation BMI is weight in kg's devided by Height in meter's square.
If a person's BMI is less than 25 that person is healthy, if it is more than 25 indicaes threat to his health condition.

This week I added GuestBook to my BMI application.
Try it at https://bitbucket.org/mani3434/a03earla

#Installation
Fork this respository or just download the code as a zipfile to get started. 
Right-click on the groupmaker.html file to view the application in your browser (e.g. Chrome). 


#Acknowledgments
https://www.cdc.gov/healthyweight/assessing/bmi/
